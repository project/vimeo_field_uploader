
-------------------------------------------------------------------------------
                               Vimeo Field Uploader
-------------------------------------------------------------------------------

The Vimeo field Uploader module allows users to Uploads videos to vimeo through entity
either Content type or media
It creates Vimeo video field which can be used in any entity.

This module required Vimeo API

Prerequisite
-------------
 * Create a App at vimeo.com, visit <https://developer.vimeo.com/apps>
   Get the Created App Authenticated.
   (It will takes few days to get authenticated by vimeo.com)
   Copy/Save the details.
    - Vimeo User Id.
    - Client ID.
    - Client Secret.
    - Access token.

Installation
-------------
 * Copy the whole vimeo field uploader directory to your modules directory
(e.g. DRUPAL_ROOT/modules) and activate the Vimeo fleld Uploader
module
* Download the Library API of Vimeo from "https://github.com/vimeo/vimeo.php".
* Go to the path "DRUPAL_ROOT/libraries"
Create a folder "vimeo-lib-api" and place the downloaded all file.
* Required to check autoload.php path
(DRUPAL_ROOT/libraries/vimeo-lib-api/autoload.php).

Configuration
--------------

 * Configuration Page.
   http://yoursite.com/admin/config/vimeo_field_uploader/Vimeofielduploader
 * On module configuration page enter the copied/saved details of App from
   https://www.vimeo.com and select the content type from which you have to
   upload the video to Vimeo.