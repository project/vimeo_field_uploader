<?php

namespace Drupal\vimeo_field_uploader\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'vimeovideo_default' widget.
 *
 * @FieldWidget(
 *   id = "vimeovideo_default",
 *   label = @Translation("Vimeo Video"),
 *   field_types = {
 *     "Vimeovideo"
 *   }
 * )
 */
class VimeovideoDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = [
      'vimeo_file_extensions' => 'mp4 ogv webm',
      'vimeo_max_filesize' => '',
    ] + parent::defaultSettings();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();

    // Make the extension list a little more human-friendly by comma-separation.
    $extensions = str_replace(' ', ', ', $settings['vimeo_file_extensions']);
    $element['vimeo_file_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed file extensions'),
      '#default_value' => $extensions,
      '#description' => $this->t('Separate extensions with a space or comma and do not include the leading dot.'),
      '#element_validate' => [[get_class($this), 'vimeoValidateExtensions']],
      '#weight' => 1,
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    $element['vimeo_max_filesize'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum upload size'),
      '#default_value' => $settings['vimeo_max_filesize'],
      '#description' => $this->t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the allowed file size to upload on vimeo . If left empty the file sizes will be limited only by PHP\'s maximum post and file upload sizes (current limit <strong>%limit</strong>).', ['%limit' => format_size(file_upload_max_size())]),
      '#size' => 10,
      '#element_validate' => [[get_class($this), 'vimeoValidateMaxFilesize']],
      '#weight' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t(
      'Extensions : @vimeo_file_extensions<br/><br/>@vimeo_max_filesize', [
        '@vimeo_file_extensions' => $this->getSetting('vimeo_file_extensions'),
        '@vimeo_max_filesize' => ($this->getSetting('vimeo_max_filesize')) ? 'Max filesize: ' . $this->getSetting('vimeo_max_filesize') : '',
      ]
    );
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $fieldTitleName = $element['#title'];

    $element = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();
    $allowedextensions = str_replace(' ', ', ', $settings['vimeo_file_extensions']);
    $ext = str_replace(',', '', $allowedextensions);
    $maxfilesize = $this->getSetting('vimeo_max_filesize');
    // Calculate max file size to upload file.
    if (!empty($maxfilesize)) {
      $unit = preg_replace('/[^bkmgtpezy]/i', '', $maxfilesize);
      $size = preg_replace('/[^0-9\.]/', '', $maxfilesize);
      if ($unit) {
        $filesize = round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
      }
      else {
        $filesize = round($size);
      }
    }
    else {
      $filesize = file_upload_max_size();
      $maxfilesize = round($filesize / 1024 / 1024, 4) . 'MB';
    }

    $element['target_id'] = [
      '#type' => 'managed_file',
      '#title' => $this->t($fieldTitleName),
      '#upload_location' => 'public://',
      '#default_value' => isset($items[$delta]->target_id) ? $items[$delta]->target_id : NULL,
      '#description' => $this->t('Upload local video to vimeo<br>Max Filesize: @maxfilesize <br> Allowed extenstion: @allowedextensions', ['@maxfilesize' => $maxfilesize, '@allowedextensions' => $allowedextensions]),
      '#upload_validators' => [
        'file_validate_extensions' => [$ext],
        'file_validate_size' => [$filesize],
      ],
      '#required' => '',
    ];

    $element['vimeovideourl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vimeo embed url'),
      '#default_value' => isset($items[$delta]->vimeovideourl) ? $items[$delta]->vimeovideourl : NULL,
      '#empty_value' => '',
      '#attributes' => ['readonly' => 'readonly'],
      '#description' => $this->t('Vimeo Embed url field is auto filled by vimeo video url when video is uploaded on vimeo'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function vimeoValidateExtensions($element, FormStateInterface $form_state) {
    if (!empty($element['#value'])) {
      $extensions = preg_replace('/([, ]+\.?)/', ' ', trim(strtolower($element['#value'])));
      $extensions = array_filter(explode(' ', $extensions));
      $extensions = implode(' ', array_unique($extensions));
      if (!preg_match('/^([a-z0-9]+([.][a-z0-9])* ?)+$/', $extensions)) {
        $form_state->setError($element, $this->t('The list of allowed extensions is not valid, be sure to exclude leading dots and to separate extensions with a comma or space.'));
      }
      else {
        $form_state->setValueForElement($element, $extensions);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function vimeoValidateMaxFilesize($element, FormStateInterface $form_state) {
    if (!empty($element['#value']) && (Bytes::toInt($element['#value']) == 0)) {
      $form_state->setError($element, $this->t('The option must contain a valid value. You may either leave the text field empty or enter a string like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes).'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    foreach ($values as &$value) {
      $value['target_id'] = !empty($value['target_id'][0]) ? $value['target_id'][0] : '0';
      $value['vimeovideourl'] = !empty($value['vimeovideourl']) ? $value['vimeovideourl'] : '';
    }
    return $value;
  }

}
