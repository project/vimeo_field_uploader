<?php

namespace Drupal\vimeo_field_uploader\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Plugin implementation of the 'Vimeovideo' field type.
 *
 * @FieldType(
 *   id = "Vimeovideo",
 *   label = @Translation("Vimeo Video"),
 *   description = @Translation("This field stores the ID of an vimeo file or embedded vimeo as an integer value."),
 *   category = @Translation("Vimeo"),
 *   default_widget = "vimeovideo_default",
 *   default_formatter = "vimeovideo_default"
 * )
 */
class VimeovideoItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['vimeovideourl'] = DataDefinition::create('string')
      ->setLabel(t('Vimeo Video'));
    $properties['target_id'] = DataDefinition::create('string')
      ->setLabel(t('Additional video metadata'))
      ->setDescription(t("Additional metadata for the uploadded or embedded video."));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'vimeovideourl' => [
          'type' => 'varchar',
          'length' => 512,
          'not null' => FALSE,
        ],
        'target_id' => [
          'description' => "Additional video metadata.",
          'type' => 'varchar',
          'length' => 512,
        ],
      ],
      'indexes' => [
        'target_id' => ['target_id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $target_id = $this->get('target_id')->getValue();
    $vimeovideourl = $this->get('vimeovideourl')->getValue();
    return empty($target_id) && empty($vimeovideourl);
  }

}
