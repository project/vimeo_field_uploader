<?php

namespace Drupal\vimeo_field_uploader\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'vimeovideo_default' formatter.
 *
 * @FieldFormatter(
 *   id = "vimeovideo_default",
 *   module = "Vimeovideo",
 *   label = @Translation("Vimeovideo"),
 *   field_types = {
 *     "Vimeovideo"
 *   }
 * )
 */
class VimeovideoDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'responsive' => TRUE,
      'width' => '854',
      'height' => '480',
      'autoplay' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['autoplay'] = [
      '#title' => $this->t('Autoplay'),
      '#type' => 'checkbox',
      '#description' => $this->t('Autoplay the Vimeo videos'),
      '#default_value' => $this->getSetting('autoplay'),
    ];
    $elements['responsive'] = [
      '#title' => $this->t('Responsive Video'),
      '#type' => 'checkbox',
      '#description' => $this->t("Make the video fill the width of it's container, adjusting to the size of the user's screen."),
      '#default_value' => $this->getSetting('responsive'),
    ];
    // Loosely match the name attribute so forms which don't have a field.
    // formatter structure (such as the WYSIWYG settings form) are also matched.
    $responsive_checked_state = [
      'visible' => [
        [
          ':input[name*="responsive"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $elements['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'number',
      '#field_suffix' => 'px',
      '#default_value' => $this->getSetting('width'),
      '#required' => TRUE,
      '#size' => 20,
      '#states' => $responsive_checked_state,
    ];
    $elements['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'number',
      '#field_suffix' => 'px',
      '#default_value' => $this->getSetting('height'),
      '#required' => TRUE,
      '#size' => 20,
      '#states' => $responsive_checked_state,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $dimensions = $this->getSetting('responsive') ? $this->t('Responsive') : $this->t('@widthx@height', ['@width' => $this->getSetting('width'), '@height' => $this->getSetting('height')]);
    $summary[] = $this->t(
      'Embedded Vimeo Video (@dimensions@autoplay).', [
        '@dimensions' => $dimensions,
        '@autoplay' => $this->getSetting('autoplay') ? $this->t(', autoplaying') : '',
      ]
    );
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $vimeoUrl = $item->get('vimeovideourl')->getValue();
      $fetchVimeoIdArr = explode('/', $vimeoUrl);
      $idCounter = count($fetchVimeoIdArr) - 1;
      $vimeoId = $fetchVimeoIdArr[$idCounter];
      if ($this->getSetting('responsive')) {
        $elements[$delta] = [
          '#type' => 'container',
          '#theme' => 'vimeo_field_uploader',
          '#url' => sprintf('https://player.vimeo.com/video/%s?autoplay=%s', $vimeoId, $this->getSetting('autoplay')),
          '#attributes' => ['class' => ['vimeo-responsive-video']],
          '#responsive' => $this->getSetting('responsive'),
        ];
      }
      else {
        $elements[$delta] = [
          '#type' => 'container',
          '#theme' => 'vimeo_field_uploader',
          '#url' => sprintf('https://player.vimeo.com/video/%s?autoplay=%s', $vimeoId, $this->getSetting('autoplay')),
          '#attributes' => ['class' => ['vimeo-responsive-video']],
          '#videowidth' => $this->getSetting('width'),
          '#videoheight' => $this->getSetting('height'),
          '#responsive' => $this->getSetting('responsive'),
        ];
      }
      return $elements;
    }
  }

}
