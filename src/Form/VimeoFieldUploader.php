<?php

namespace Drupal\vimeo_field_uploader\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VimeoFieldUploader.
 */
class VimeoFieldUploader extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vimeo_field_uploader.vimeofielduploader',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vimeo_field_uploader';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vimeo_field_uploader.vimeofielduploader');
    $form['vimeo_auth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Vimeo Video Upload Configuration'),
    ];
    $form['vimeo_auth']['client_identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Identifier'),
      '#description' => $this->t('Vimeo Clinet Identifier'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('client_identifier'),
    ];
    $form['vimeo_auth']['client_secrets'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secrets'),
      '#description' => $this->t('Vimeo Client Secrets'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('client_secrets'),
    ];
    $form['vimeo_auth']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your new Access token'),
      '#description' => $this->t('Enter generated (Your new Access token)'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('access_token'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('vimeo_field_uploader.vimeofielduploader')
      ->set('client_identifier', $form_state->getValue('client_identifier'))
      ->set('client_secrets', $form_state->getValue('client_secrets'))
      ->set('access_token', $form_state->getValue('access_token'))
      ->save();
  }

}
